using NUnit.Framework;
using RestSharp;
using RestSharpNetCoreTemplate.Bases;
using RestSharpNetCoreTemplate.Requests.Filters;
using System;
using System.Collections.Generic;
using System.Text;

namespace RestSharpNetCoreTemplate.Tests.Filters
{    
    [TestFixture]
    public class FindAllFiltersTests : TestBase
    {
        [Test, Order(23)]
        [Obsolete]
        public void BuscarTodosOsFiltrosExistentes()
        {
            string filters = "filters";
            string statusCodeEsperado = "OK";

            FindAllFiltersRequest findAllFiltersRequest = new FindAllFiltersRequest(filters);

            IRestResponse<dynamic> response = findAllFiltersRequest.ExecuteRequest();

            Assert.AreEqual(statusCodeEsperado, response.StatusCode.ToString());
        }
    }
}