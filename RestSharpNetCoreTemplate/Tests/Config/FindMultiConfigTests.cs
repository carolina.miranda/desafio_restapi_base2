using NUnit.Framework;
using RestSharp;
using RestSharpNetCoreTemplate.Bases;
using RestSharpNetCoreTemplate.Requests.Config;
using System;
using System.Collections.Generic;
using System.Text;

namespace RestSharpNetCoreTemplate.Tests.Config
{    
    [TestFixture]
    public class FindMultiConfigTests : TestBase
    {
        [Test, Order(26)]
        [Obsolete]
        public void BuscarMultiplasConfiguracoes()
        {
            string csv = "csv_separator";
            string statusCodeEsperado = "OK";

            FindMultiConfigRequest findMultiConfigRequest = new FindMultiConfigRequest(csv);

            IRestResponse<dynamic> response = findMultiConfigRequest.ExecuteRequest();

            Assert.AreEqual(statusCodeEsperado, response.StatusCode.ToString());
        }
    }
}