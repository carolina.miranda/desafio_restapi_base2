using RestSharpNetCoreTemplate.Bases;
using RestSharpNetCoreTemplate.Helpers;
using RestSharpNetCoreTemplate.Requests.Users;
using NUnit.Framework;
using RestSharp;
using System;
using System.Collections;

namespace RestSharpNetCoreTemplate.Tests.DDT
{
    public class DataDrivenTests : TestBase
    {
        AddUserRequest addUserRequest = new AddUserRequest();


        [Test, TestCaseSource("CadastrarUsuariosDDT")]
        [Obsolete]
        public void CadastrarUsuariosDDT()
        {
            //IEnumerable testData = GeneralHelpers.ReturnCSVData(GeneralHelpers.ReturnProjectPath() + "Resources/TestData/CadastrarUsuarios.csv");
            
            string[] testData = GeneralHelpers.TesteCSV();

            string statusCodeEsperado = "Created";

            string id = testData.ToString();

            Console.WriteLine(id);
            //string username = testData[1].ToString();
            //string password = testData[2].ToString();
            //string real_name = testData[3].ToString();
            //string email = testData[4].ToString();
            
            addUserRequest.setJsonBody();
            IRestResponse<dynamic> response = addUserRequest.ExecuteRequest();
            
            Assert.AreEqual(statusCodeEsperado, response.StatusCode.ToString());

            
        }

        /*public static IEnumerable CadastrarUsuariosDDT()
        {
            return GeneralHelpers.ReturnCSVData(GeneralHelpers.ReturnProjectPath() + "Resources/TestData/CadastrarUsuarios.csv");
        }*/
    }
}